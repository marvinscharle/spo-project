class Stock < ActiveRecord::Base
  belongs_to :material

  def volume
    self[:direction]*self[:quantity]
  end
end
