class Delivery < ActiveRecord::Base
  belongs_to :quote
  belongs_to :material

  def serializable_hash
    super(:only => [:material_id, :quantity, :price, :logistic_cost]).merge(:shipping_date => shipping_date, :delivery_date => delivery_date)
  end

  def shipping_date
    self[:date]-(delivery_time+1).days
  end

  def delivery_date
    self[:date]
  end

  def calculate_price!
    # Calculate the price, based on the following dimensions: customer, mass_discount, min_price
    if quote.customer.nil?
      customer_discount = 1.0
    else
      customer_discount = 1.0-(quote.customer.discount.to_f/100)
    end

    suggested_price = material.price*(material.discount_for_quantity(quote.quantity)*customer_discount)

    self[:price] = (suggested_price < material.minimal_price ? material.minimal_price : suggested_price)*self[:quantity]
  end

  def calculate_logistic_cost!
    # Calculate the logistic costs based on customer and plant
    if quote.customer.nil?
      # No customer, no shipping
      self[:logistic_cost] = 0
      self[:delivery_time] = 0
    else
      # Get logistic data based on plant distance
      logistic_data = LogisticParameter.get_by_distance(quote.plant.distance)
      range_cost = logistic_data.cost_per_kilometer*quote.plant.distance/1000
      package_cost = (self[:quantity].to_f/material.items_per_package).ceil*logistic_data.package_base_cost
      self[:logistic_cost] = range_cost+package_cost
      self[:delivery_time] = material.delivery_time
    end
  end

  def replace_material!
    # Replaces the volume of the material, shipping in this order
    # if date is in replacement_time
    if self[:date]-(quote.material.replacement_time+1).days > Time.now.to_date
      rx = ReplacementOrder.create_with_delivery_date(self[:date], quote.material, self[:quantity])
    else
      rx = ReplacementOrder.create_with_order_date(Time.now.to_date, quote.material, self[:quantity])
    end

    HistoryEntry.create :message => "Replacement Order for Material ##{material.id} (#{rx.quantity} units, on #{rx.delivery_date}) created", :type => 'fa-rotate-left'

    rx
  end
end
