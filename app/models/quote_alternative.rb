class QuoteAlternative < ActiveRecord::Base
  belongs_to :quote, :foreign_key => :quote_id, :class_name => Quote
  belongs_to :alternative, :foreign_key => :alternative_id, :class_name => Quote
end
