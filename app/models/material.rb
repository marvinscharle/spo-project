class Material < ActiveRecord::Base
  has_many :stocks
  has_many :quotes
  has_many :volume_discounts

  # Validate presence of attributes
  validates_presence_of :short_name, :price, :replacement_time, :delivery_time, :production_cost, :minimal_price, :items_per_package

  def serializable_hash
    super(:only => [:id, :short_name, :long_name, :delivery_time, :updated_at]).merge(:current_stock => stock)
  end

  def stock
    stock_at Time.now.to_date
  end

  # Calculate the suggested stock based on the last 10 and next 30 days
  def suggested_stock
    quantity_transactions = []
    (-10..30).to_a.each do |i|
      day = Time.now.to_date + i.days
      stocks.where(:date => day, :direction => -1).each {|stock| quantity_transactions << stock.quantity}
    end
    if quantity_transactions.length > 0
      quantity_average = (quantity_transactions.inject{|sum, element| sum+element}.to_f/quantity_transactions.length).ceil
    else
      quantity_average = 0
    end

    quantity_average
  end

  def discount_for_quantity(quantity)
    discount = volume_discounts.where(['quantity <= ?', quantity]).order('quantity DESC').first
    return 1.0 if discount.nil?

    (1.0-(discount.discount.to_f/100)).to_i
  end

  # @param date [Date]
  def stock_at (date)
    return $redis.get("Simulation:StockAt:#{self[:id]}:#{date}").to_i if $redis.exists "Simulation:StockAt:#{self[:id]}:#{date}"

    stock = 0

    # Check if data set exists for yesterday in redis
    yesterday = Time.now.to_date-1.day
    if $redis.exists("Simulation:StockAt:#{self[:id]}:#{yesterday}")
      # Start from yesterday, reference to the future
      stock = $redis.get("Simulation:StockAt:#{self[:id]}:#{yesterday}").to_i
      stocks.where('date <= ?', date).where('date > ?', yesterday).each do |transaction|
        stock += transaction.volume
      end
    else
      stocks.where('date <= ?', date).each do |transaction|
        stock += transaction.volume
      end
    end

    # Cache in redis
    $redis.set("Simulation:StockAt:#{self[:id]}:#{date}", stock)
    if date >= Time.now.to_date
      # Expire after 10 minutes if today or future date
      $redis.expire("Simulation:StockAt:#{self[:id]}:#{date}", 10.minutes)
    end

    stock
  end

  def replacement_orders_at (date)
    stock = 0
    ReplacementOrder.where(:material_id => self[:id], :order_date => date).each do |order|
      stock += order.quantity
    end

    stock
  end

  def outgoing_deliveries_at (date)
    stock = 0
    Stock.where(:material_id => self[:id], :direction => 1, :date => date).each do |delivery|
      stock += delivery.quantity
    end

    stock
  end

  def incoming_deliveries_at (date)
    stock = 0
    Stock.where(:material_id => self[:id], :direction => -1, :date => date).each do |delivery|
      stock += delivery.quantity
    end

    stock
  end

  def reduce_stock_at (date, by, reference='')
    stock = stocks.create(:direction => -1, :quantity => by, :change_ref => reference, :date => date)

    return nil unless stock.save

    stocks
  end

  def increase_stock_at (date, by, reference='')
    # Cancel if in replacement_time
    return nil if date-(self[:replacement_time]+1).days < Time.now.to_date

    stock = stocks.create(:direction => 1, :quantity => by, :change_ref => reference, :date => date)

    return nil unless stock.save

    stocks
  end

  def in_replacement_time? (date)
    Time.now.to_date >= date-(self[:replacement_time]+1).days
  end

  # @return Quote
  def add_quote
    quote = quotes.create(:valid_till => Time.now.to_datetime+$quote_lifetime, :active_quote => true)
    o = [('a'..'z'), ('A'..'Z'), (0..9)].map { |i| i.to_a }.flatten
    quote.access_id = (0...4).map{ o[rand(o.length)] }.join

    return nil unless quote.save

    quote
  end

  def check_availability (quantity, target_date)
    # Check if target_date is outside the replacement_time
    return true if target_date > (Time.now.to_date+(self[:replacement_time]+1).days)

    # Fetch available stock between target_date and replacement_time
    stock_availability = (0..((Time.now.to_date+(self[:replacement_time]+1).days)-target_date).to_i).map{|el| date = target_date+el.days; stock_at(date)}

    # Reduce the stock by quantity, check if array includes negative values -> if yes, requested quantity is not available
    stock_availability.map{|stock_at_day| stock_at_day-quantity}.reject { |element| element < 0 }.length == stock_availability.length
  end

  def self.get(id)
    Material.where(:id => id, :active => true).first
  end
end
