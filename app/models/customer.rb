class Customer < ActiveRecord::Base
  has_many :customer_plants, :class_name => Plant
  has_many :quotes

  def serializable_hash
    super(:only => [:id])
  end

  def plants
    customer_plants.where(:active => true)
  end
end
