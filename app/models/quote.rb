class Quote < ActiveRecord::Base
  belongs_to :material
  belongs_to :customer
  has_one :order
  belongs_to :plant
  has_many :deliveries

  def serializable_hash
    x = super(:only => [:id, :access_id, :call_id, :valid_till])
    if plant.nil?
      x.merge!(:distance => nil)
    else
      x.merge!(:distance => plant.distance)
    end
    x.merge!(:quantity => quantity, :price => price, :logistic_cost => logistic_cost, :total => total_price, :deliveries => deliveries.map {|delivery| delivery.serializable_hash})

    # Map alternatives
    alt_quotes = alternatives
    x.merge!(:alternatives => alt_quotes.map{|alternative| alternative.serializable_hash}) if alt_quotes.length > 0

    x
  end

  def has_order?
    !order.nil?
  end

  def price
    price = 0
    deliveries.each {|delivery| price += delivery.price}

    price
  end

  def active?
    return false unless active_quote?
    return parent_quote.active? unless parent_quote.nil?

    self[:valid_till] > Time.now.to_datetime
  end

  def calculate_quantity!
    quantity = 0
    deliveries.each {|delivery| quantity += delivery.quantity}

    self[:quantity] = quantity
  end

  def block_material!
    if parent_quote.nil?
      delivery_dates = {}
      (alternatives+[self]).each do |quote|
        quote.deliveries.each do |delivery|
          if delivery_dates.has_key? delivery.date.to_s
            delivery_dates[delivery.date.to_s] = delivery.quantity if delivery.quantity > delivery_dates[delivery.date.to_s]
          else
            delivery_dates[delivery.date.to_s] = delivery.quantity
          end
        end
      end

      delivery_dates.each do |date, quantity|
        material.stocks.create(:direction => -1, :quantity => quantity, :change_ref => "quote:#{self[:id]}", :date => Date.parse(date))

        HistoryEntry.create(:message => "#{quantity} units of Material #{material.id} blocked for quote ##{self[:id]} at #{date}", :type => 'fa-thumb-tack')
      end
    else
      parent_quote.block_material!
    end
  end

  def free_material!
    if parent_quote.nil?
      Stock.where(:change_ref => "quote:#{self[:id]}").destroy_all
      (alternatives+[self]).each{|quote| quote.active_quote = false; quote.save}

      HistoryEntry.create(:message => "Blocked Stock for Quote ##{self[:id]} has been released.", :type => 'fa-check-square')
    else
      parent_quote.free_material!
    end
  end

  def logistic_cost
    costs = 0
    deliveries.each {|delivery| costs += delivery.logistic_cost}

    costs
  end

  def total_price
    price+logistic_cost
  end

  def alternatives
    Quote.find_by_sql(['SELECT quotes.* FROM quote_alternatives, quotes WHERE quote_alternatives.quote_id = ? AND quotes.id = quote_alternatives.alternative_id', self[:id]])
  end

  def parent_quote
    Quote.find_by_sql(['SELECT quotes.* FROM quote_alternatives, quotes WHERE quote_alternatives.alternative_id = ? AND quotes.id = quote_alternatives.quote_id', self[:id]]).first
  end

  def add_alternative
    quote = Quote.create(:date => self[:date], :quantity => self[:quantity], :material_id => self[:material_id], :active_quote => true)
    o = [('a'..'z'), ('A'..'Z'), (0..9)].map { |i| i.to_a }.flatten
    quote.access_id = (0...4).map{ o[rand(o.length)] }.join

    quote.save
    QuoteAlternative.create(:quote => self, :alternative => quote).save

    quote
  end

  def add_delivery(options={})
    options.merge!(:quote_id => self[:id])

    delivery = Delivery.create options
    delivery.calculate_logistic_cost!
    delivery.calculate_price!

    delivery
  end

  def add_order(external_id, quantity)
    puts 1
    return nil if has_order?
    puts 2
    return nil unless active?
    puts 3

    free_material!

    new_order = Order.create(:quantity => quantity, :external_id => external_id, :quote_id => self[:id])
    o = [('a'..'z'), ('A'..'Z'), (0..9)].map { |i| i.to_a }.flatten
    new_order.access_id = (0...4).map{ o[rand(o.length)] }.join

    return nil unless new_order.save

    if quantity == self[:quantity]
      deliveries.each do |delivery|
        delivery.replace_material!
        material.reduce_stock_at(delivery.date, delivery.quantity, "order:#{self[:id]}")
        HistoryEntry.create(:message => "New Delivery for Order ##{new_order.id} scheduled", :type => 'fa-truck')
      end
    else
      # Check out and replace materials
      shipped_units = 0

      # For each planned delivery, checkout and replace materials while quantity does not extend ordered quantity
      deliveries.each do |delivery|
        if shipped_units < quantity
          mx = shipped_units+delivery.quantity
          if mx > quantity
            delivery.quantity = delivery.quantity-(mx-quantity)
            delivery.save
          end

          delivery.replace_material!
          material.reduce_stock_at(delivery.date, delivery.quantity, "order:#{self[:id]}")
          shipped_units += delivery.quantity

          HistoryEntry.create(:message => "New Delivery for Order ##{new_order.id} scheduled", :type => 'fa-truck')
        else
          delivery.destroy
        end
      end
    end

    new_order
  end
end
