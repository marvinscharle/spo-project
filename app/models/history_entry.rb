class HistoryEntry
  attr_reader :created_at, :message, :type, :id, :url

  def created_at
    Time.at(@created_at.to_i).to_datetime
  end

  def id
    @id.to_i
  end

  def initialize (data={})
    data.each do |key, value|
      instance_variable_set "@#{key}", value
    end
  end

  def serializable_hash
    {:id => self.id, :type => self.type, :message => self.message, :created_at => self.created_at, :url => self.url}
  end

  def self.get (id)
    data = JSON.parse($redis.lindex('Simulation:History', id-1))
    self.new data
  end

  def self.all
    $redis.lrange('Simulation:History', 0, -1).map{|entry| self.new(JSON.parse(entry))}
  end

  def self.find_in_range (start, stop)
    $redis.lrange('Simulation:History', start, stop).map{|entry| self.new(JSON.parse(entry))}
  end

  def self.create (data={})
    x = {:message => data[:message], :type => data[:type], :created_at => Time.now.to_i, :id => $redis.incr('Simulation:HistoryId'), :url => data[:url]}

    # Add to redis
    $redis.rpush('Simulation:History', x.to_json)
    $redis.publish('Simulation:History:Channel', x.to_json)

    self.new x
  end
end