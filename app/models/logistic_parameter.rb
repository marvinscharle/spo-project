class LogisticParameter < ActiveRecord::Base

  def self.get_by_distance (distance)
    result = where(['max_distance >= ?', distance]).order('max_distance ASC').first
    return result unless result.nil?

    where(:max_distance => -1).first
  end

  def self.default
    where(:max_distance => -1).first
  end
end
