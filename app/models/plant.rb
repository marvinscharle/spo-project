class Plant < ActiveRecord::Base
  belongs_to :customer

  def serializable_hash
    super(:only => [:id, :active, :address])
  end
end
