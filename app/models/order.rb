class Order < ActiveRecord::Base
  belongs_to :quote

  def serializable_hash
    super(:only => [:id, :external_id, :access_id, :quantity, :date]).merge(:material_id => quote.material_id)
  end
end
