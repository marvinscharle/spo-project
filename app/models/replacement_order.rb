class ReplacementOrder < ActiveRecord::Base
  belongs_to :material

  def self.create_with_delivery_date(date, material, quantity)
    order_date = date-(material.replacement_time+1).days
    return nil if order_date < Time.now.to_date

    rp_order = create(:material_id => material.id, :quantity => quantity, :delivery_date => date, :order_date => order_date, :cost => quantity*material.production_cost)
    rp_order.generate_stock!

    rp_order
  end

  def self.create_with_order_date(date, material, quantity)
    delivery_date = date+(material.replacement_time+1).days

    rp_order = create(:material_id => material.id, :quantity => quantity, :delivery_date => delivery_date, :order_date => date, :cost => quantity*material.production_cost)
    rp_order.generate_stock!

    rp_order
  end

  def generate_stock!
    material.increase_stock_at(self[:delivery_date], quantity, "rp_order:#{self[:id]}")
  end
end
