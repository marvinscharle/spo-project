class ApplicationController < ActionController::Base
  class ErrorMessage < RuntimeError
    attr_reader :data, :error_code
    def initialize (message, data={})
      super(message)
      @data = data
      @error_code = (data[:error_code].nil? ? 500 : data[:error_code])
    end
  end

  rescue_from ErrorMessage do |error|
    @error = error
    respond_to do |f|
      f.html { render 'ui/error.html', status: error.error_code }
      f.json {render :json => {:error => true, :message => error.message}, status: error.error_code}
    end
  end

  def error! (message='', data={})
    raise ErrorMessage.new(message,data)
  end

  def default_url_options
    {:host => 'sim.hellenzon.de', :port => 80}
  end
end
