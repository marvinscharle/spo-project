class CustomerController < ApplicationController
  # :access_id
  def details
    @customer = Customer.find_by_access_id params[:access_id]

    error! 'Customer not found', :error_code => 404 if @customer.nil?

    respond_to do |f|
      f.html
      f.json { render :json => @customer.serializable_hash.merge(:plants => @customer.plants.map {|plant| {:id => plant.id, :url => plant_details_url(:customer_access_id => @customer.access_id, :plant_id => plant.id, :format => 'json')} })}
    end
  end

  def change
    if params.has_key? :id
      customer = Customer.find_by_id params[:id]
      error! 'Customer not found', :error_code => 404 if customer.nil?
    else
      customer = Customer.new
    end

    params.each do |key, value|
      next unless customer.has_attribute? key
      next if key == :id
      next if key == :access_id

      customer[key] = value
    end

    o = [('a'..'z'), ('A'..'Z'), (0..9)].map { |i| i.to_a }.flatten
    customer.access_id = (0...4).map{ o[rand(o.length)] }.join

    customer.save!
    HistoryEntry.create :message => "Customer ##{customer.id} created", :type => 'fa-users', :url => customer_details_path(:access_id => customer.access_id)

    redirect_to customer_details_path(:access_id => customer.access_id)
  end

  # :customer_access_id
  def plants
    @customer = Customer.find_by_access_id params[:customer_access_id]

    error! 'Customer not found', :error_code => 404 if @customer.nil?

    respond_to do |f|
      f.html
      f.json { render :json => @customer.plants.map {|plant| plant.serializable_hash } }
    end
  end

  def list
    @customers = Customer.all
    respond_to do |f|
      f.html
    end
  end

  def view_create
    respond_to do |f|
      f.html
    end
  end
end
