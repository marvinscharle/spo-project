class OrderController < ApplicationController

  # :id, :quote_id, :material_id, :quantity
  def create
    error! 'Wrong input', :error_code => 400 unless params.has_key?(:id) && params.has_key?(:quote_id) && params.has_key?(:material_id) && params.has_key?(:quantity)

    quote = Quote.find_by_id params[:quote_id]
    error! 'Quote not found', :error_code => 404 if quote.nil?

    error! 'Material not associated with quote', :error_code => 400 unless quote.material_id == params[:material_id].to_i
    error! 'Ordered quantity exceeds quote', :error_code => 400 if params[:quantity].to_i > quote.quantity

    error! 'Order already created for this quote', :error_code => 400 if quote.has_order?

    # Bestellung erzeugen
    order = quote.add_order(params[:id].to_i, params[:quantity].to_i)
    error! 'Error creating order', :error_code => 500 if order.nil?

    HistoryEntry.create :message => "New order added for material ##{quote.material_id}", :type => 'fa-star', :url => order_details_path(:access_id => order.access_id)

    respond_to do |f|
      f.html { redirect_to order_list_path }
      f.json { render :json => order.serializable_hash }
    end
  end

  def get
    order = Order.find_by_access_id params[:access_id]
    error! 'Order not found', :error_code => 404 if order.nil?

    respond_to do |f|
      f.json { render :json => order.serializable_hash }
      f.html { redirect_to quote_details_path(:access_id => order.quote.access_id) }
    end
  end

  def list
    @orders = Order.all

    respond_to do|f|
      f.html
    end
  end

  def view_create
    @quote = Quote.find_by_access_id params[:access_id]
    error! 'Quote not found', :error_code => 404 if @quote.nil?
    error! 'Quote inactive', :error_code => 400 unless @quote.active?
    respond_to do |f|
      f.html
    end
  end

end
