class UiController < ApplicationController
  def view_home
  end

  def statistics
    @new_orders = {}
    @new_quotes = {}
    quote_order = {}
    (-5..10).to_a.map{|i| [@new_orders, quote_order, @new_quotes].each{|k| k[(Time.now.to_date+i.days).to_s] = 0}}

    @profit = (-5..10).to_a.map do |i|
      day = Time.now.to_date + i.days
      replacement_cost = 0
      ReplacementOrder.where(:order_date => day).each{|rp_order| replacement_cost += rp_order.cost.to_i}
      income = 0
      Quote.where(:date => day).each do |quote|
        @new_quotes[day.to_s] += 1
        next unless quote.has_order?
        @new_orders[day.to_s] += 1
        income += quote.price
      end

      quote_order[day.to_s] = @new_orders[day.to_s].to_f/@new_quotes[day.to_s].to_f unless @new_quotes[day.to_s] == 0

      {:day => day.to_s, :income => income.to_f/100, :loss => replacement_cost.to_f/100, :profit => (income-replacement_cost).to_f/100}
    end

    @new_quotes_orders = (-5..10).to_a.map do |i|
      day = Time.now.to_date + i.days
      {:day => day, :quotes => @new_quotes[day.to_s], :orders => @new_orders[day.to_s]}
    end

    @quote_order_translation = []
    quote_order.each{|k,v| @quote_order_translation.push({:day => k, :translation => (v*100).to_i})}

    @new_customers = (-5..10).to_a.map do |i|
      day = Time.now.to_date + i.days
      day_end = Time.now.to_date + (i+1).days
      count = Customer.where(['created_at < ?', day_end]).count

      {:day => day.to_s, :customers => count}
    end
  end

  def bender
    respond_to do |f|
      f.html {render :status => 418}
    end
  end
end
