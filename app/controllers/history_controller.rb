class HistoryController < ApplicationController
  def list
    @entries = HistoryEntry.find_in_range(-100, -1).reverse

    respond_to do |f|
      f.html
    end
  end
end
