class PlantController < ApplicationController

  # :customer_access_id, :plant_id
  def details
    customer = Customer.find_by_access_id params[:customer_access_id]
    error! 'Plant not found', :error_code => 404 if customer.nil?

    plant = customer.customer_plants.where(:id => params[:plant_id]).first
    error! 'Plant not found', :error_code => 404 if plant.nil?

    respond_to do |f|
      f.json { render :json => plant.serializable_hash}
    end
  end

  # :customer_access_id, :address
  def create
    customer = Customer.find_by_access_id params[:customer_access_id]
    error! 'Customer not found', :error_code => 404 if customer.nil?
    error! 'Missing address of plant', :error_code => 400 unless params.has_key? :address

    # Create plant as inactive, asynchronously calculate distance
    plant = customer.plants.create(:address => params[:address], :active => false)

    response['Location'] = plant_details_url(:customer_access_id => params[:customer_access_id], :plant_id => plant.id, :format => :json)
    respond_to do |f|
      f.html { redirect_to customer_details_path(:access_id => customer.access_id) }
      f.json { render :json => plant.serializable_hash, status: 201}
    end
  end

  def remove
    customer = Customer.find_by_access_id params[:customer_access_id]
    error! 'Plant not found', :error_code => 404 if customer.nil?

    plant = customer.plants.where(:id => params[:plant_id]).first
    error! 'Plant not found', :error_code => 404 if plant.nil?

    plant.active = false
    error! 'Plant could not be deleted', :error_code => 500 unless plant.save

    respond_to do |f|
      f.json { render :json => plant.serializable_hash}
    end
  end

  def view_create
    @customer = Customer.find_by_access_id params[:customer_access_id]
    error! 'Customer not found', :error_code => 404 if @customer.nil?

    respond_to do |f|
      f.html
    end
  end
end
