class QuoteController < ApplicationController
  # :material_id, :date (yyyymmdd), :call_id, :quantity, (:customer, :plant)
  def create
    error! 'Wrong input', :error_code => 400 unless params.has_key?(:material_id) && params.has_key?(:date) && params.has_key?(:call_id) && params.has_key?(:quantity)
    begin
      date = Date.strptime(params[:date].gsub('-', ''), '%Y%m%d')
    rescue
      error! 'Wrong date format', :error_code => 400
    end

    error! 'Date must be in future', :error_code => 400 unless date > Time.now.to_date

    existing_quote = Quote.where(:material_id => params[:material_id], :date => date, :call_id => params[:call_id].to_i, :requested_quantity => params[:quantity].to_i).first

    if existing_quote.nil?
      # Create new Quote (HTTP 201 [Created])
      # Fetch requested material
      material = Material.get params[:material_id]
      error! 'Material not found', :error_code => 404 if material.nil?

      # Check if delivery date is acceptable based on delivery time
      error! 'Delivery date not possible due to shipping time', :error_code => 400 unless date > Time.now.to_date+(material.delivery_time+1).day


      # Fetch customer and plant if given
      if params.has_key?(:customer) && params.has_key?(:plant) && params[:customer] != ''
        customer = Customer.find_by_access_id params[:customer]
        error! 'Customer not found', :error_code => 400 if customer.nil?

        plant = customer.customer_plants.where(:id => params[:plant]).first
        error! 'Plant not found', :error_code => 400 if plant.nil?
      end

      # Set requested quantity
      requested_quantity = params[:quantity].to_i

      # Create primary quote object
      quote = material.add_quote
      error! 'Unable to create quote' if quote.nil?

      quote.date = date
      quote.call_id = params[:call_id].to_i
      quote.requested_quantity = requested_quantity
      unless plant.nil?
        quote.customer_id = customer.id
        quote.plant_id = plant.id
      end
      error! 'Unable to create quote' unless quote.save

      # Primary quote only includes stock available at requested date.
      # Check if requested date is in replacement time -> check for availability needed
      if material.in_replacement_time?(date)

        # get stock at requested date
        stock_at_date = material.stock_at(date)

        # if stock_at_date is larger than or equal requested stock, only create primary quote
        if stock_at_date >= requested_quantity
          quote.add_delivery(:date => date, :quantity => requested_quantity, :material => material).save
        else
          # Primary quote will only include stock available at giving time spot
          quote.add_delivery(:date => date, :quantity => stock_at_date, :material => material).save

          # Add alternative quotes
          # Alternative 1: split delivery, unless first delivery quantity > 0
          if stock_at_date > 0
            alternative_quote = quote.add_alternative
            unless plant.nil?
              alternative_quote.customer_id = customer.id
              alternative_quote.plant_id = plant.id
              alternative_quote.save
            end

            # First delivery includes all stock available at requested time point
            alternative_quote.add_delivery(:date => date, :quantity => stock_at_date, :material => material).save

            # Create additional deliveries till stock is complete
            delivered_units = stock_at_date
            target_date = date.clone
            latest_added = date.clone

            while delivered_units < requested_quantity
              next_delivering_units = 0

              # Bundle availability in the next 14 days
              14.times do
                target_date = target_date+1.day

                # check if outside replacement time
                if material.in_replacement_time?(target_date)
                  available_stock = material.stock_at(target_date)
                else
                  available_stock = requested_quantity
                end

                if available_stock > next_delivering_units
                  next_delivering_units = available_stock-delivered_units
                  latest_added = target_date.clone

                  if next_delivering_units >= (requested_quantity-delivered_units)
                    next_delivering_units = (requested_quantity-delivered_units)
                    break
                  end
                end
              end

              # Add new delivery, if next_delivering_units > 0
              next unless next_delivering_units > 0
              alternative_quote.add_delivery(:date => latest_added, :quantity => next_delivering_units, :material => material).save
              delivered_units = delivered_units+next_delivering_units
            end

            # Save alternative quote
            alternative_quote.calculate_quantity!
            alternative_quote.save
          end


          # Second alternative: move delivery to later time point with complete requested units
          # Condition: next 30 days
          target_date = date.clone

          30.times do
            target_date = target_date+1.day

            # check if outside replacement time
            if material.in_replacement_time?(target_date)
              available_stock = material.stock_at(target_date)
            else
              available_stock = requested_quantity
            end

            # if available_stock > requested, exit loop, create delivery
            if available_stock >= requested_quantity

              # Add alternative quote
              alternative_quote = quote.add_alternative
              unless plant.nil?
                alternative_quote.customer_id = customer.id
                alternative_quote.plant_id = plant.id
                alternative_quote.save
              end

              alternative_quote.add_delivery(:date => target_date, :quantity => requested_quantity, :material => material).save
              alternative_quote.calculate_quantity!
              alternative_quote.save
              break
            end
          end
        end
      else
        # If not in replacement time, entire stock is available
        quote.add_delivery(:date => date, :quantity => requested_quantity, :material => material).save
      end

      quote.calculate_quantity!
      quote.block_material!
      quote.save

      HistoryEntry.create :message => "New quote added for material ##{quote.material_id}", :type => 'fa-asterisk', :url => quote_details_path(:access_id => quote.access_id)

      response['Location'] = quote_details_url(:access_id => quote.access_id, :format => :json)
      respond_to do |f|
        f.json { render :json => quote.serializable_hash, status: 201}
        f.html { redirect_to quote_list_path }
      end
    else
      # Angebot ausgeben (Status 303: See other)
      response['Location'] = quote_details_url(:access_id => existing_quote.access_id, :format => :json)
      respond_to do |f|
        f.json { render :json => existing_quote.serializable_hash, status: 303}
        f.html { redirect_to quote_list_path }
      end
    end
  end

  def get
    @quote = Quote.find_by_access_id params[:access_id]

    error! 'Quote not found', :error_code => 404 if @quote.nil?

    respond_to do |f|
      f.json { render :json => @quote.serializable_hash}
      f.html
    end
  end

  def view_create
    respond_to do |f|
      f.html
    end
  end

  def list
    @quotes = Quote.where('valid_till IS NOT NULL')
    respond_to do |f|
      f.html
    end
  end
end
