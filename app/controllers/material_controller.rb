class MaterialController < ApplicationController
  def list
    materials = Material.where(:active => true)
    @materials = materials
    respond_to do |f|
      f.html
      f.json { render :json => materials.map { |material| {:id => material.id, :details_url => material_details_url(:material_id => material.id, :format => :json)}} }
    end
  end

  # :material_id
  def get
    @material = Material.get params[:material_id]
    error! 'Material not found', :error_code => 404 if @material.nil?

    @material_stock = (0..10).to_a.map{|i| day = Time.now.to_date + i.days; {:day => day.to_s, :ordered_incoming => @material.replacement_orders_at(day), :deliveries_at => @material.outgoing_deliveries_at(day), :deliveries_incoming => @material.incoming_deliveries_at(day)}}

    @stock_over_time = (-5..10).to_a.map do |i|
      day = Time.now.to_date + i.days
      {:day => day, :stock => @material.stock_at(day)}
    end

    respond_to do |f|
      f.json { render :json => @material.serializable_hash }
      f.html
    end
  end

  def view_create
    respond_to do |f|
      f.html
    end
  end

  # :id
  def change
    if params.has_key? :id
      material = Material.find_by_id params[:id]
      error! 'Material not found', :error_code => 404 if material.nil?
    else
      material = Material.new
    end

    params.each do |key, value|
      next unless material.has_attribute? key
      next if key == :id

      material[key] = value
    end

    material.save!
    HistoryEntry.create :message => "Material ##{material.id} created", :type => 'fa-cubes', :url => material_details_path(:material_id => material.id)

    redirect_to material_details_path(:material_id => material.id)
  end

  # :material_id
  def remove
    material = Material.get params[:material_id]
    error! 'Material not found', :error_code => 404 if material.nil?

    material.active = false
    material.save

    HistoryEntry.create :message => "Material ##{material.id} deactivated", :type => 'fa-cubes'

    redirect_to material_list_path(:message => "Material #{material.id} has been deactivated")
  end
end
