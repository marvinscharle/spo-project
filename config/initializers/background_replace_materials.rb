Thread.new do
  loop do

    begin
      puts 'Begin automatically replacing materials'
      # Iterate over materials, calculate suggested stock
      Material.where(:active => true).each do |material|
        suggested_stock = material.suggested_stock

        # Starting with the the replacement time, for 10 days in the future, replace stock with suggested stock
        ((material.replacement_time+1)..(material.replacement_time+10)).to_a.each do |i|
          day = Time.now.to_date+i.days
          stock_at_day = material.stock_at(day)
          if stock_at_day < suggested_stock
            stock_to_replace = suggested_stock-stock_at_day
            rx = ReplacementOrder.create_with_delivery_date(day, material, stock_to_replace)
            puts "Material #{material.id} for #{day} replaced with suggested stock"
            HistoryEntry.create :message => "Auto Replacement Order for Material ##{material.id} (#{rx.quantity} units, on #{rx.delivery_date}) created", :type => 'fa-rotate-left'
          end
        end
      end
    rescue
      puts 'Error occurred replacing materials'
    end


    sleep 1800
  end
end