# Our plant
$current_position = 'Marienstraße 20, 89518 Heidenheim, Germany'

require 'net/http'

Thread.new do
  loop do

    Plant.where(:distance => nil, :active => false).each do |plant|
      puts "Processing plant #{plant.id}"

      begin
        # Fetching distance from Google Maps API
        puts 'Fetching Google Maps API....'
        url = URI.parse(URI::encode("http://maps.googleapis.com/maps/api/distancematrix/json?origins=#{$current_position}&destinations=#{plant.address}"))
        request = Net::HTTP::Get.new(url.to_s)
        result = Net::HTTP.start(url.host, url.port) {|http| http.request(request)}

        json = JSON.parse(result.body)
        result_set = json['rows'][0]['elements'][0]
        raise ArgumentError unless result_set['status'] == 'OK'

        plant.distance = result_set['distance']['value']
        plant.active = true
        plant.save!

        puts "Plant #{plant.id} saved"
      rescue
        plant.distance = -1
        plant.save

        puts "Plant #{plant.id}:processing failed"
      end
    end

    sleep 10

  end
end