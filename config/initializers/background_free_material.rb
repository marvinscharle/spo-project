$quote_lifetime = 30.minutes

Thread.new do
  loop do

    Quote.where(:active_quote => true).where('valid_till < ?', Time.now.to_datetime).each do |quote|
      begin
        quote.free_material!
        puts "Successful: Freeing materials for quote #{quote.id}"
      rescue
        puts "Freeing materials for quote #{quote.id} failed"
      end
    end

    sleep 60
  end
end