SPO::Application.routes.draw do

  root 'ui#view_home'


  get 'materials' => 'material#list', as: :material_list
  get 'material/create' => 'material#view_create', as: :material_view_create
  post 'material/create' => 'material#change', as: :material_create
  get 'material/:material_id' => 'material#get', as: :material_details
  post 'material/:id' => 'material#change', as: :material_change
  get 'material/:material_id/delete' => 'material#remove', as: :material_remove

  get 'quote/create' => 'quote#view_create', as: :quote_view_create
  post 'quote/create' => 'quote#create', as: :quote_create
  get 'quote/:access_id' => 'quote#get', as: :quote_details
  get 'quote/:access_id/order' => 'order#view_create', as: :order_view_create
  get 'quotes' => 'quote#list', as: :quote_list
  get 'quote' => 'quote#create', as: :quote_create_api

  post 'order' => 'order#create', as: :order_create
  get 'order/:access_id'  => 'order#get', as: :order_details
  get 'orders' => 'order#list', as: :order_list

  get 'customers' => 'customer#list', as: :customer_list
  get 'customer/create' => 'customer#view_create', as: :customer_view_create
  post 'customer/create' => 'customer#change', as: :customer_create
  get 'customer/:access_id' => 'customer#details', as: :customer_details
  get 'customer/:customer_access_id/plants' => 'customer#plants', as: :customer_plants

  get 'customer/:customer_access_id/plant' => 'plant#view_create', as: :plant_view_create
  post 'customer/:customer_access_id/plant' => 'plant#create', as: :plant_create
  get 'customer/:customer_access_id/plant/:plant_id' => 'plant#details', as: :plant_details
  delete 'customer/:customer_access_id/plant/:plant_id' => 'plant#remove', as: :plant_remove

  get 'history' => 'history#list', as: :history
  get 'statistics' => 'ui#statistics', as: :statistic
  get 'bender' => 'ui#bender', as: :bender

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
