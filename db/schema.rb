# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140814131315) do

  create_table "customers", force: true do |t|
    t.string   "name"
    t.integer  "discount"
    t.string   "access_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "deliveries", force: true do |t|
    t.integer  "quantity"
    t.date     "date"
    t.integer  "material_id"
    t.integer  "quote_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "price"
    t.integer  "logistic_cost"
    t.integer  "delivery_time"
  end

  create_table "logistic_parameters", force: true do |t|
    t.integer  "max_distance"
    t.integer  "additional_delivery_time"
    t.integer  "cost_per_kilometer"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "package_base_cost"
  end

  create_table "materials", force: true do |t|
    t.string   "short_name"
    t.text     "long_name"
    t.integer  "delivery_time"
    t.integer  "price"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.date     "date"
    t.integer  "replacement_time"
    t.integer  "production_cost"
    t.integer  "minimal_price"
    t.integer  "items_per_package"
    t.boolean  "active",            default: true
  end

  create_table "orders", force: true do |t|
    t.integer  "quantity"
    t.integer  "quote_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "access_id"
    t.integer  "external_id"
  end

  create_table "plants", force: true do |t|
    t.string   "address"
    t.integer  "distance"
    t.integer  "customer_id"
    t.boolean  "active"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "quote_alternatives", force: true do |t|
    t.string   "quote_id"
    t.string   "alternative_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "quotes", force: true do |t|
    t.string   "access_id"
    t.integer  "call_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "material_id"
    t.date     "date"
    t.integer  "customer_id"
    t.integer  "plant_id"
    t.integer  "quantity"
    t.datetime "valid_till"
    t.boolean  "active_quote"
    t.integer  "requested_quantity"
  end

  create_table "replacement_orders", force: true do |t|
    t.integer  "material_id"
    t.integer  "quantity"
    t.date     "order_date"
    t.date     "delivery_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "stocks", force: true do |t|
    t.integer  "direction"
    t.integer  "quantity"
    t.string   "change_ref"
    t.date     "date"
    t.integer  "material_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "volume_discounts", force: true do |t|
    t.integer  "material_id"
    t.integer  "quantity"
    t.integer  "discount"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
