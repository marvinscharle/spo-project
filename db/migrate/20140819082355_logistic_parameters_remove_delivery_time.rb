class LogisticParametersRemoveDeliveryTime < ActiveRecord::Migration
  def change
    remove_column :logistic_parameters, :additional_delivery_time
  end
end
