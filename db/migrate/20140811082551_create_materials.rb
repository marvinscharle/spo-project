class CreateMaterials < ActiveRecord::Migration
  def change
    create_table :materials do |t|
      t.string :short_name
      t.text :long_name
      t.integer :delivery_time
      t.integer :price

      t.timestamps
    end
  end
end
