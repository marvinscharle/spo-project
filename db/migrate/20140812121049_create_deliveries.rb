class CreateDeliveries < ActiveRecord::Migration
  def change
    create_table :deliveries do |t|
      t.integer :quantity
      t.date :date
      t.integer :material_id
      t.integer :quote_id

      t.timestamps
    end

    remove_column :quotes, :date
    remove_column :quotes, :quantity
  end
end
