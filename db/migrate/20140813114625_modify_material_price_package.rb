class ModifyMaterialPricePackage < ActiveRecord::Migration
  def change
    add_column :materials, :replacement_time, :integer
    add_column :materials, :production_cost, :integer
    add_column :materials, :minimal_price, :integer
    add_column :materials, :items_per_package, :integer

    add_column :logistic_parameters, :package_base_cost, :integer
  end
end
