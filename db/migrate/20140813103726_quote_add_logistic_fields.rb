class QuoteAddLogisticFields < ActiveRecord::Migration
  def change
    add_column :deliveries, :price, :integer
    add_column :deliveries, :logistic_cost, :integer
    add_column :deliveries, :delivery_time, :integer
  end
end
