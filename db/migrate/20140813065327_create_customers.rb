class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.string :name
      t.integer :discount
      t.string :access_id

      t.timestamps
    end
  end
end
