class CreateQuotes < ActiveRecord::Migration
  def change
    create_table :quotes do |t|
      t.date :date
      t.integer :quantity
      t.string :access_id
      t.integer :call_id

      t.timestamps
    end
  end
end
