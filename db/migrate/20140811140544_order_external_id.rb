class OrderExternalId < ActiveRecord::Migration
  def change
    add_column :orders, :external_id, :integer
  end
end
