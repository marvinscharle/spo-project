class QuoteActiveFlag < ActiveRecord::Migration
  def change
    add_column :quotes, :active_quote, :boolean
  end
end
