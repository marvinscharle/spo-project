class MaterialActive < ActiveRecord::Migration
  def change
    add_column :materials, :active, :boolean, :default => true
  end
end
