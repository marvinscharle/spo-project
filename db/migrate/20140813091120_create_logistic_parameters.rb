class CreateLogisticParameters < ActiveRecord::Migration
  def change
    create_table :logistic_parameters do |t|
      t.integer :max_distance
      t.integer :additional_delivery_time
      t.integer :cost_per_kilometer

      t.timestamps
    end
  end
end
