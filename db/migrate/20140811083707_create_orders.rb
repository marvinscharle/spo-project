class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.date :date
      t.integer :quantity
      t.integer :quote_id

      t.timestamps
    end
  end
end
