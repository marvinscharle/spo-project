class CreatePlants < ActiveRecord::Migration
  def change
    create_table :plants do |t|
      t.string :address
      t.integer :distance
      t.integer :customer_id
      t.boolean :active

      t.timestamps
    end

    add_column :quotes, :customer_id, :integer
    add_column :quotes, :plant_id, :integer
  end
end
