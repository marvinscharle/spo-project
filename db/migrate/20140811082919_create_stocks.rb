class CreateStocks < ActiveRecord::Migration
  def change
    create_table :stocks do |t|
      t.integer :direction
      t.integer :quantity
      t.string :change_ref
      t.date :date
      t.integer :material_id

      t.timestamps
    end
  end
end
