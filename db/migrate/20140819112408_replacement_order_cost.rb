class ReplacementOrderCost < ActiveRecord::Migration
  def change
    add_column :replacement_orders, :cost, :integer
  end
end
