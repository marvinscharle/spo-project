class QuoteValidTill < ActiveRecord::Migration
  def change
    add_column :quotes, :valid_till, :datetime
  end
end
