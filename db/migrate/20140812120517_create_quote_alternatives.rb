class CreateQuoteAlternatives < ActiveRecord::Migration
  def change
    create_table :quote_alternatives do |t|
      t.string :quote_id
      t.string :alternative_id

      t.timestamps
    end
  end
end
