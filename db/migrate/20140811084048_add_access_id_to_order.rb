class AddAccessIdToOrder < ActiveRecord::Migration
  def change
    add_column :orders, :access_id, :string
  end
end
