class AddMaterialIdToQuote < ActiveRecord::Migration
  def change
    add_column :quotes, :material_id, :integer
  end
end
