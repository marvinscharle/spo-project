class QuoteAddRequestedQuantity < ActiveRecord::Migration
  def change
    add_column :quotes, :requested_quantity, :integer
  end
end
