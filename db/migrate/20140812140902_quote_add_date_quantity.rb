class QuoteAddDateQuantity < ActiveRecord::Migration
  def change
    add_column :quotes, :date, :date
    add_column :quotes, :quantity, :integer
  end
end
