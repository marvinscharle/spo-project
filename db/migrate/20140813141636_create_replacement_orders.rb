class CreateReplacementOrders < ActiveRecord::Migration
  def change
    create_table :replacement_orders do |t|
      t.integer :material_id
      t.integer :quantity
      t.date :order_date
      t.date :delivery_date

      t.timestamps
    end
  end
end
