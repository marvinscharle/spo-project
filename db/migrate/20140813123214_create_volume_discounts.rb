class CreateVolumeDiscounts < ActiveRecord::Migration
  def change
    create_table :volume_discounts do |t|
      t.integer :material_id
      t.integer :quantity
      t.integer :discount

      t.timestamps
    end
  end
end
